package com.stocks.crunchbase.loader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public class DayPriceDataLoader {

  @Value(value = "/testdata")
  private String directoryPath;

  @Autowired
  private DayPriceLoader dayPriceLoader;

  public DayPriceDataLoader() {}

  public DayPriceDataLoader(DayPriceLoader dayPriceLoader, String directoryPath) {
    this.dayPriceLoader = dayPriceLoader;
    this.directoryPath = directoryPath;
  }

  public void load() {
    File directory = new File(getClass().getResource(directoryPath).getPath());
    if (directory.exists()) {
      loadDataFromFiles(directory);
    }
  }

  private void loadDataFromFiles(File directory) {
    File[] files = directory.listFiles();
    if (files == null) return;

    for (File file : files) {
      dayPriceLoader.load(file);
    }
  }
}
