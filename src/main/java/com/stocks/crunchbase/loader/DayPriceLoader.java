package com.stocks.crunchbase.loader;

import com.stocks.crunchbase.model.DayPrice;
import com.stocks.crunchbase.model.Equity;
import com.stocks.crunchbase.parser.DayPriceCsvParser;
import com.stocks.crunchbase.repository.DayPriceRepository;
import com.stocks.crunchbase.repository.EquityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
public class DayPriceLoader {

  private DayPriceCsvParser dayPriceCsvParser;
  private DayPriceRepository dayPriceRepository;
  private EquityRepository equityRepository;

  @Autowired
  public DayPriceLoader(DayPriceCsvParser dayPriceCsvParser, DayPriceRepository dayPriceRepository, EquityRepository equityRepository) {
    this.dayPriceCsvParser = dayPriceCsvParser;
    this.dayPriceRepository = dayPriceRepository;
    this.equityRepository = equityRepository;
  }

  public void load(File file) {
    List<DayPrice> dayPrices = dayPriceCsvParser.parse(file);
    LocalDate tradingDate = dayPrices.stream().findFirst().map(DayPrice::getTradingDate).get();

    if (dayPriceRepository.findByTradingDate(tradingDate).isEmpty())
      dayPrices.forEach((dayPrice) -> saveDayPrice(dayPrice));
  }

  private void saveDayPrice(DayPrice dayPrice) {
    Equity equity = dayPrice.getEquity();
    Equity existingEquity = equityRepository.findByIdentifier(equity.getIdentifier());
    if (existingEquity != null) {
      dayPrice.setEquity(existingEquity);
    } else {
      equityRepository.save(dayPrice.getEquity());
    }

    dayPriceRepository.save(dayPrice);
  }
}
