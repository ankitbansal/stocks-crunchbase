package com.stocks.crunchbase.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

@Table(name = "day_prices")
@Entity
@Getter
@NoArgsConstructor
public class DayPrice {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  @Column(name = "open_price")
  private BigDecimal openPrice;
  @Column(name = "close_price")
  private BigDecimal closePrice;
  @Column(name = "high_price")
  private BigDecimal highPrice;
  @Column(name = "low_price")
  private BigDecimal lowPrice;
  @Column(name = "prev_close")
  private BigDecimal prevClose;
  @Column(name="total_traded_quantity")
  private BigInteger totalTradedQuantity;
  @Column(name="total_traded_value")
  private BigDecimal totalTradedValue;

  @Type(type = "localDateType")
  @Column(name="trading_date")
  private LocalDate tradingDate;

  @ManyToOne
  @JoinColumn(name = "equity_id")
  private Equity equity;

  public DayPrice(BigDecimal openPrice, BigDecimal closePrice, BigDecimal highPrice,
                  BigDecimal lowPrice, BigDecimal prevClose, BigInteger totalTradedQuantity,
                  BigDecimal totalTradedValue, LocalDate tradingDate, Equity equity) {
    this.openPrice = openPrice;
    this.closePrice = closePrice;
    this.highPrice = highPrice;
    this.lowPrice = lowPrice;
    this.prevClose = prevClose;
    this.totalTradedQuantity = totalTradedQuantity;
    this.totalTradedValue = totalTradedValue;
    this.tradingDate = tradingDate;
    this.equity = equity;
  }

  public void setEquity(Equity equity) {
    this.equity = equity;
  }
}
