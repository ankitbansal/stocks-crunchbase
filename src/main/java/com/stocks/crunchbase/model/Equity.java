package com.stocks.crunchbase.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name = "equities")
@Getter
@Entity
@NoArgsConstructor
@EqualsAndHashCode(of = {"identifier"})
public class Equity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private String name;
  private String identifier;
  private String series;

  public Equity(String name, String identifier, String series) {
    this.name = name;
    this.identifier = identifier;
    this.series = series;
  }

}
