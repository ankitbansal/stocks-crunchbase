package com.stocks.crunchbase.parser;

import com.stocks.crunchbase.model.DayPrice;
import com.stocks.crunchbase.model.Equity;
import com.stocks.crunchbase.util.DateUtils;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static com.stocks.crunchbase.util.Throwables.propagate;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.csv.CSVFormat.EXCEL;

@Component
public class DayPriceCsvParser {

  public static final String SERIES = "SERIES";
  public static final String EQUITY = "EQ";
  public static final String SYMBOL = "SYMBOL";
  public static final String IDENTIFIER = "ISIN";
  public static final String OPEN = "OPEN";
  public static final String CLOSE = "CLOSE";
  public static final String HIGH = "HIGH";
  public static final String LOW = "LOW";
  public static final String PREV_CLOSE = "PREVCLOSE";
  public static final String TOTAL_TRADED_QTY = "TOTTRDQTY";
  public static final String TOTAL_TRADED_VAL = "TOTTRDVAL";
  public static final String TIMESTAMP = "TIMESTAMP";

  public List<DayPrice> parse(File file) {
    FileReader fileReader = propagate(() -> new FileReader(file));
    List<CSVRecord> records = newArrayList(propagate(() -> EXCEL.withHeader().parse(fileReader)));
    return records.stream()
        .filter(record -> EQUITY.equals(record.get(SERIES)))
        .map(this::convertToDayPrice).collect(toList());
  }

  private DayPrice convertToDayPrice(CSVRecord record) {
    Equity equity = convertToEquity(record);

    BigDecimal openPrice = new BigDecimal(record.get(OPEN));
    BigDecimal closePrice = new BigDecimal(record.get(CLOSE));
    BigDecimal highPrice = new BigDecimal(record.get(HIGH));
    BigDecimal lowPrice = new BigDecimal(record.get(LOW));
    BigDecimal prevClose = new BigDecimal(record.get(PREV_CLOSE));
    BigInteger totalTradedQuantity = new BigInteger(record.get(TOTAL_TRADED_QTY));
    BigDecimal totalTradedValue = new BigDecimal(record.get(TOTAL_TRADED_VAL));
    LocalDate date = DateUtils.parseDateWithMonthInCaps(record.get(TIMESTAMP));

    return new DayPrice(openPrice, closePrice, highPrice, lowPrice, prevClose, totalTradedQuantity, totalTradedValue, date, equity);
  }

  private Equity convertToEquity(CSVRecord record) {
    String name = record.get(SYMBOL);
    String identifier = record.get(IDENTIFIER);
    String series = record.get(SERIES);
    return new Equity(name, identifier, series);
  }

}
