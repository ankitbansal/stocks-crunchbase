package com.stocks.crunchbase.repository;

import com.stocks.crunchbase.model.DayPrice;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;

public interface DayPriceRepository extends CrudRepository<DayPrice, Long> {
  List<DayPrice> findByTradingDate(LocalDate tradingDate);
}
