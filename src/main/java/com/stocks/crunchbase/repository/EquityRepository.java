package com.stocks.crunchbase.repository;

import com.stocks.crunchbase.model.Equity;
import org.springframework.data.repository.CrudRepository;

public interface EquityRepository extends CrudRepository<Equity, Long> {
  Equity findByIdentifier(String identifier);
}
