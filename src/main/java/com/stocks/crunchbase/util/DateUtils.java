package com.stocks.crunchbase.util;

import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.WordUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateUtils {

  public static LocalDate parseDateWithMonthInCaps(String dateString) {
    String capitalized = WordUtils.capitalize(dateString.toLowerCase(), new char[]{'-'});
    return LocalDate.parse(capitalized, DateTimeFormatter.ofPattern("dd-MMM-yyyy"));
  }
}
