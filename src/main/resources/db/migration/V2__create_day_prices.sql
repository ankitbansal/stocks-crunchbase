CREATE TABLE day_prices
(
  ID bigint(20) NOT NULL AUTO_INCREMENT,
  equity_id bigint(20) NOT NULL,
  open_price DECIMAL(19,6) NOT NULL,
  high_price DECIMAL(19,6) NOT NULL,
  close_price DECIMAL(19,6) NOT NULL,
  low_price DECIMAL(19,6) NOT NULL,
  prev_close DECIMAL(19,6) NOT NULL,
  total_traded_quantity bigint(20)  NOT NULL,
  total_traded_value DECIMAL(19,6)  NOT NULL,
  trading_date DATE NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT FK_day_prices_equity_id FOREIGN KEY (equity_id) REFERENCES equities (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;