package com.stocks.crunchbase.builder;

import com.stocks.crunchbase.model.DayPrice;
import com.stocks.crunchbase.model.Equity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

public class DayPriceBuilder {
  private BigDecimal openPrice = new BigDecimal(100);
  private BigDecimal closePrice = new BigDecimal(101);
  private BigDecimal highPrice = new BigDecimal(102);
  private BigDecimal lowPrice = new BigDecimal(99);
  private BigDecimal prevClose = new BigDecimal(99.5);
  private BigInteger totalTradedQuantity = new BigInteger("10");
  private BigDecimal totalTradedValue = new BigDecimal(1000);
  private LocalDate tradingDate = LocalDate.of(2013, 10, 1);
  private Equity equity = new Equity("INFOSYS", "ISIN12343", "EQ");

  public DayPrice build() {
    return new DayPrice(openPrice, closePrice, highPrice, lowPrice, prevClose, totalTradedQuantity, totalTradedValue, tradingDate, equity);
  }

  public DayPriceBuilder withTradingDate(LocalDate tradingDate) {
    this.tradingDate = tradingDate;
    return this;
  }

  public DayPriceBuilder withEquity(Equity equity) {
    this.equity = equity;
    return this;
  }
}
