package com.stocks.crunchbase.builder;

import com.stocks.crunchbase.model.Equity;

public class EquityBuilder {

  private String name = "infosys";
  private String identifier = "ISIN1234324";
  private String series = "EQ";

  public Equity build() {
    return new Equity(name, identifier, series);
  }

  public EquityBuilder withIdentifier(String identifier) {
    this.identifier = identifier;
    return this;
  }
}
