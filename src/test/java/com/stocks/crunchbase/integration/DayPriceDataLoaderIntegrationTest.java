package com.stocks.crunchbase.integration;

import com.stocks.crunchbase.loader.DayPriceDataLoader;
import com.stocks.crunchbase.util.BaseRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class DayPriceDataLoaderIntegrationTest extends BaseRepository {

  @Autowired
  private DayPriceDataLoader dayPriceDataLoader;

  @Before
  public void setUp() {
    super.setUp();
  }

  @Test
  public void shouldSaveDayPricesFromFiles() throws Exception {
    dayPriceDataLoader.load();

    assertThat(dayPriceRepository.count(), is(5L));
    assertThat(equityRepository.count(), is(3L));
  }
}
