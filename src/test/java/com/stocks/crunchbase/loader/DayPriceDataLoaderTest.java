package com.stocks.crunchbase.loader;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class DayPriceDataLoaderTest {

  private DayPriceDataLoader dayPriceDataLoader;

  @Mock
  private DayPriceLoader dayPriceLoader;
  private String directoryPath = "/testdata";

  @Before
  public void setUp() {
    dayPriceDataLoader = new DayPriceDataLoader(dayPriceLoader, directoryPath);
  }

  @Test
  public void shouldLoadDataFromFiles() throws Exception {
    dayPriceDataLoader.load();

    verify(dayPriceLoader).load(new File(getAbsolutePath(directoryPath) + "/equity1.csv"));
    verify(dayPriceLoader).load(new File(getAbsolutePath(directoryPath) + "/equity2.csv"));
  }

  private String getAbsolutePath(String path) {
    return getClass().getResource(path).getPath();
  }
}
