package com.stocks.crunchbase.loader;

import com.stocks.crunchbase.builder.DayPriceBuilder;
import com.stocks.crunchbase.builder.EquityBuilder;
import com.stocks.crunchbase.model.DayPrice;
import com.stocks.crunchbase.model.Equity;
import com.stocks.crunchbase.parser.DayPriceCsvParser;
import com.stocks.crunchbase.repository.DayPriceRepository;
import com.stocks.crunchbase.repository.EquityRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.time.LocalDate;

import static java.util.Arrays.asList;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DayPriceLoaderTest {

  @InjectMocks
  private DayPriceLoader dayPriceLoader;

  @Mock
  private DayPriceCsvParser dayPriceCsvParser;
  @Mock
  private DayPriceRepository dayPriceRepository;
  @Mock
  private EquityRepository equityRepository;

  @Test
  public void shouldStoreDayPricesAndEquityWhenEquityNotExistsAlready() throws Exception {
    File equityCsv = new File("somePath");
    String identifier = "ISIN123423";
    Equity equity = new EquityBuilder().withIdentifier(identifier).build();
    DayPrice dayPrice = new DayPriceBuilder().withTradingDate(LocalDate.now()).withEquity(equity).build();

    when(equityRepository.findByIdentifier(identifier)).thenReturn(null);
    when(dayPriceCsvParser.parse(equityCsv)).thenReturn(asList(dayPrice));

    dayPriceLoader.load(equityCsv);
    verify(equityRepository).save(equity);
    verify(dayPriceRepository).save(dayPrice);
  }

  @Test
  public void shouldStoreDayPricesOnlyWhenEquityExists() throws Exception {
    File equityCsv = new File("somePath");
    String identifier = "ISIN123423";
    Equity equity = new EquityBuilder().withIdentifier(identifier).build();
    DayPrice dayPrice = new DayPriceBuilder().withTradingDate(LocalDate.now()).withEquity(equity).build();

    when(equityRepository.findByIdentifier(identifier)).thenReturn(equity);
    when(dayPriceCsvParser.parse(equityCsv)).thenReturn(asList(dayPrice));

    dayPriceLoader.load(equityCsv);
    verify(dayPriceRepository).save(dayPrice);
    verify(equityRepository, never()).save(any(Equity.class));
  }

  @Test
  public void shouldNotTryToReinsertDayPriceDataWhenAlreadyImported() throws Exception {
    File equityCsv = new File("somePath");
    LocalDate tradingDate = LocalDate.now();

    DayPrice dayPrice = new DayPriceBuilder().withTradingDate(tradingDate).build();

    when(dayPriceRepository.findByTradingDate(tradingDate)).thenReturn(asList(dayPrice));
    when(dayPriceCsvParser.parse(equityCsv)).thenReturn(asList(dayPrice));

    dayPriceLoader.load(equityCsv);

    verify(dayPriceRepository, never()).save(dayPrice);
  }
}
