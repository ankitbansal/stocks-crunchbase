package com.stocks.crunchbase.parser;

import com.stocks.crunchbase.model.DayPrice;
import com.stocks.crunchbase.model.Equity;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class DayPriceCsvParserTest {

  @Test
  public void shouldParseEquityDataWhileIgnoringOthers() throws FileNotFoundException {
    DayPriceCsvParser dayPriceCsvParser = new DayPriceCsvParser();
    File file = getFile("/fixtures/equity.csv");
    List<DayPrice> dayPrices = dayPriceCsvParser.parse(file);
    assertThat(dayPrices.size(), is(3));
    List<String> names = dayPrices.stream().map((dayPrice) -> dayPrice.getEquity().getName()).collect(Collectors.toList());
    assertThat(names, containsInAnyOrder("20MICRONS", "3IINFOTECH", "3MINDIA"));
  }

  @Test
  public void shouldConstructEquityCorrectly() throws Exception {
    DayPriceCsvParser dayPriceCsvParser = new DayPriceCsvParser();
    File file = getFile("/fixtures/equity.csv");
    List<DayPrice> dayPrices = dayPriceCsvParser.parse(file);

    DayPrice dayPrice = dayPrices.stream().filter(record -> record.getEquity().getName().equals("20MICRONS")).findFirst().get();
    Equity equity = dayPrice.getEquity();
    assertThat(equity.getName(), is("20MICRONS"));
    assertThat(equity.getIdentifier(), is("INE144J01027"));
    assertThat(equity.getSeries(), is("EQ"));

    assertThat(dayPrice.getOpenPrice(), is(new BigDecimal("32")));
    assertThat(dayPrice.getHighPrice(), is(new BigDecimal("32")));
    assertThat(dayPrice.getLowPrice(), is(new BigDecimal("30.35")));
    assertThat(dayPrice.getClosePrice(), is(new BigDecimal("30.75")));
    assertThat(dayPrice.getPrevClose(), is(new BigDecimal("31.3")));
    assertThat(dayPrice.getTotalTradedQuantity(), is(new BigInteger("46677")));
    assertThat(dayPrice.getTotalTradedValue(), is(new BigDecimal("1434840.6")));
    assertThat(dayPrice.getTradingDate(), is(LocalDate.of(2013, 4, 1)));
  }

  private File getFile(String path) {
    return new File(getClass().getResource(path).getPath());
  }
}
