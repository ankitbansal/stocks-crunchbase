package com.stocks.crunchbase.repository;

import com.stocks.crunchbase.builder.DayPriceBuilder;
import com.stocks.crunchbase.builder.EquityBuilder;
import com.stocks.crunchbase.model.DayPrice;
import com.stocks.crunchbase.model.Equity;
import com.stocks.crunchbase.util.BaseRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static java.time.LocalDate.of;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@RunWith(SpringJUnit4ClassRunner.class)
public class DayPriceRepositoryTest extends BaseRepository {

  @Before
  public void setUp() {
    super.setUp();
  }

  @Test
  public void shouldSaveDayPrice() throws Exception {
    Equity equity = new EquityBuilder().build();
    equityRepository.save(equity);
    DayPrice dayPrice = new DayPriceBuilder().withEquity(equity).build();
    dayPriceRepository.save(dayPrice);

    assertThat(dayPriceRepository.count(), is(1L));
  }

  @Test
  public void shouldFindDayPricesByTradingDate() throws Exception {
    Equity equity = new EquityBuilder().build();
    equityRepository.save(equity);

    DayPrice dayPrice1 = new DayPriceBuilder().withTradingDate(of(2014, 4, 15)).withEquity(equity).build();
    DayPrice dayPrice2 = new DayPriceBuilder().withTradingDate(of(2014, 4, 16)).withEquity(equity).build();

    dayPriceRepository.save(asList(dayPrice1, dayPrice2));
    assertThat(dayPriceRepository.findByTradingDate(of(2014, 4, 15)).size(), is(1));
    assertThat(dayPriceRepository.findByTradingDate(of(2014, 4, 14)).size(), is(0));
  }
}
