package com.stocks.crunchbase.repository;

import com.stocks.crunchbase.builder.EquityBuilder;
import com.stocks.crunchbase.model.Equity;
import com.stocks.crunchbase.util.BaseRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
public class EquityRepositoryTest extends BaseRepository {

  @Autowired
  private EquityRepository equityRepository;

  @Before
  public void setUp() {
    super.setUp();
  }

  @Test
  public void shouldFindEquityByIdentifier() throws Exception {
    String identifier1 = "ISIN12345";
    Equity equity = new EquityBuilder().withIdentifier(identifier1).build();
    equityRepository.save(equity);

    assertThat(equityRepository.findByIdentifier(identifier1), is(equity));
    assertNull(equityRepository.findByIdentifier("something else"));
  }
}
