package com.stocks.crunchbase.util;

import com.stocks.crunchbase.repository.DayPriceRepository;
import com.stocks.crunchbase.repository.EquityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(
    {"classpath:configuration/services-config.xml"})
public class BaseRepository {

  @Autowired
  protected DayPriceRepository dayPriceRepository;

  @Autowired
  protected EquityRepository equityRepository;

  public void clearAllData() {
    deleteDayPrices();
  }

  private  void deleteDayPrices() {
    dayPriceRepository.deleteAll();
    equityRepository.deleteAll();
  }

  public void setUp() {
    clearAllData();
  }
}
